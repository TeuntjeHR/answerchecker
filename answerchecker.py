from os import system
import platform
import time

class color:
    GREEN = "\033[32;1m"
    FAIL = '\033[91m'
    END = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'
    GRAY = "\033[90m"
    BLINK = "\033[5m"
    LIGHTMAGENTA = "\033[95m"
    YELLOW = "\033[33m"

def GetFile():
    filename = ""

    while(filename == ""):
        filename = ""
        Exam = input(" Which option would you like to go for? ")
        #Choose the file
        if Exam == "1":
            filename = "Exams/Answers/4_1920_mock.txt"
        elif Exam == "2":
            filename = "Exams/Answers/4_1819_regular.txt"
        elif Exam == "3":
            filename = "Exams/Answers/3_1920_regular.txt"
        elif Exam == "4":
            filename = "Exams/Answers/3_1920_resit.txt"
        elif Exam == "5":
            filename = "Exams/Answers/3_2021_regular.txt"
        elif Exam == "6":
            filename = "Exams/Answers/2_1920_mock.txt"
        elif Exam == "7":
            filename = "Exams/Answers/2_1920_regular.txt"
        elif Exam == "8":
            filename = "Exams/Answers/2_1920_resit.txt"
        elif Exam == "9":
            filename = "Exams/Answers/2_2021_regular.txt"
        elif Exam == "debug":
            filename = "Exams/Answers/Testanswers.txt"
        else:
            print(f"{color.FAIL} Error exam doesn't exist{color.END}")

    #Open the answersheeeeee
    try:
        print(f"\n Alright option {Exam} it is!\n Good luck!\n")
        return open(filename)
    except:
        print(f"{color.FAIL} Error file doesn't exist{color.END}")
        GetFile()

# check OS and define clear name
if platform.system() == "Linux":
    clear = lambda: system('clear')
else:
    clear = lambda: system('cls')

clear()

print(f"{color.BLINK} {color.LIGHTMAGENTA}")
print(r"""
   _     __    __ _     __    __ __    __ _    
  |_||\|(_ | ||_ |_)   /  |_||_ /  |/ |_ |_)   
  | || |__)|^||__| \   \__| ||__\__|\ |__| \   
                                               """)
print(f"{color.END}")

#Ask what file to open
print(" Welcome,")
print(" what exam would you like to be making today?")
print(f"{color.GRAY}----------------------------------------------{color.END}")
print("  Analysis 4:")
print("  Option 1: Mock exam 19-20")
print("  Option 2: Regular exam 18-19")
print()
print("  Analysis 3:")
print("  Option 3: Regular exam 19-20")
print("  Option 4: Retake exam 19-20")
print("  Option 5: Regular exam 20-21")
print()
print("  Analysis 2:")
print("  Option 6: Mock exam 19-20")
print("  Option 7: Regular exam 19-20")
print("  Option 8: Resit exam 19-20")
print("  Option 9: Regular exam 20-21")
print(f"{color.GRAY}----------------------------------------------{color.END}")


file = GetFile()
time.sleep(2)
clear()

#Dictionaries
yourAnswers = {}
yourMistakes = {}
answerSheet = {}

#Counters
currentQuestion = 1
score = 0
mistakes = 0

#Iterate trough lines in answersheeeeee
for answer in file:

    #Ask for user input
    answerInput = input(f" {currentQuestion} : ").upper()
    answer = answer.strip("1234567890. \n")[0]
    #Save the answers
    yourAnswers[currentQuestion] = answerInput
    answerSheet[currentQuestion] = answer

    #Check if input is correct
    if answerInput == answer:
        print(f"{color.GREEN} Correct!{color.END}")
        score += 1

    else:
        mistakes += 1
        print(f"{color.FAIL} Wrong, mistake: {mistakes}{color.END}")
        yourMistakes[currentQuestion] = answerInput

    #Go to next question
    currentQuestion += 1

#Close the answersheet
file.close()

clear()

print("\n Your answers versus the correct answers:")

#Iterate through and print all answers
for x, y in yourAnswers.items():
    if y == answerSheet[x]:
        print(f"  Question {x}: {y}, {color.GREEN}PASS{color.END}")
    else:
        print(f"  Question {x}: {y}, {color.FAIL}FAIL: EXPECTED {answerSheet[x]}{color.END}")

print("\n All your incorrect answers:")

#Iterate through and print all wrong answers
if len(yourMistakes) == 0:
    print(f"  {color.YELLOW}Congratulations!{color.END} All answers were correct")
else:
    for x, y in yourMistakes.items():
        print(f"  Question {x}: {y} Should be {answerSheet[x]}")

percentage = score / len(answerSheet)

if percentage >= 2/3:
    grade = f"{color.GREEN}PASS{color.END}"
else:
    grade = f"{color.FAIL}FAIL{color.END}"


print(f"\n Score {score}/{len(answerSheet)} | Test result: {grade} | Mistakes: {mistakes}\n")